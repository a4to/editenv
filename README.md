# Editenv

Editenv is a CLI utility that allows users to manage environment variables in a simple and organized manner. It also offers the convenience of copying environment variable values directly to the clipboard.

## Features
- Define environment variables in a simple, consolidated file.
- Copy environment variable values to the clipboard with a simple command.

## Installation

Provide installation instructions here.

## Usage

`editenv -c <variable>` - Copies the value of `<variable>` to the clipboard.

`editenv -h` - Shows help information.

## Autocomplete

Editenv supports shell autocomplete for Zsh and Bash. This feature can be enabled by following the instructions in the `autocomplete` directory.

## DWM fix (Preventing not-sourced popup)

In some cases, such as running the script from a dwm keybinding, for example:

      { MODKEY,         			XK_period,	spawn,		SHCMD(TERMINAL " -e editenv") },


or other instances where the `EDITENV_ISSOURCED` or `EDITENV_PATHFILE` vars are not set, a warning will be displayed. To prevent this popup, the `--ignore-sourced-warning` flag can be added to the command, e.g. `editenv --ignore-sourced-warning`, or:

      { MODKEY,         			XK_period,	spawn,		SHCMD(TERMINAL " -e editenv --ignore-sourced-warning") },



For any other information, refer to the man page (`man editenv`).

## License

E
